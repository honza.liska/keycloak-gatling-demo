package keycloakdemo

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SimpleScenario extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:8080")
    .inferHtmlResources()
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .contentTypeHeader("application/x-www-form-urlencoded")
    .userAgentHeader("PostmanRuntime/7.26.5")

  val headers_0 = Map(
    "Cache-Control" -> "no-cache",
    "Postman-Token" -> "65875835-7850-45b6-9338-7fbcb744b385")



  val scn = scenario("SimpleScenario")
    .exec(http("request_0")
      .post("/auth/realms/smsgw/protocol/openid-connect/token")
      .headers(headers_0)
      .formParam("grant_type", "password")
      .formParam("client_id", "sms-gw")
      .formParam("client_secret", "9be3f12d-1122-48f5-bc15-f3e4df942b0f")
      .formParam("username", "test-sender")
      .formParam("password", "test"))

  setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}
