package keycloakdemo
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SimpleScenario2 extends Simulation {

	val httpProtocol = http
		.baseUrl("http://localhost:8080")
		.inferHtmlResources()
		.acceptHeader("*/*")
		.acceptEncodingHeader("gzip, deflate")
		.contentTypeHeader("application/x-www-form-urlencoded")

	val headers_0 = Map(
		"Cache-Control" -> "no-cache")

	val scn = scenario("SimpleScenario2")
		.exec(http("request_0")
			.post("/auth/realms/smsgw/protocol/openid-connect/token")
			.headers(headers_0)
			.formParam("grant_type", "password")
			.formParam("client_id", "sms-gw")
			.formParam("client_secret", "9be3f12d-1122-48f5-bc15-f3e4df942b0f")
			.formParam("username", "test-sender")
			.formParam("password", "test").check(status.is(200), jsonPath("$.access_token").saveAs("accessToken")))

	setUp(scn.inject(atOnceUsers(5))).protocols(httpProtocol)
}
