package keycloakdemo

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SendSmsScenario extends Simulation {
  val requestCount = 1;
  val keycloakUrl = "https://keycloaktest.sazkamobil.cleverlance.com/auth"
  val anonymousService = "/anonymous"
  val smsSenderService = "/sendSms"
  val clientId = "sms-gw"
  val clientSecret = "91509d8e-ff65-4957-990f-be8e2786b1bd"
  val requestPause = 500 milliseconds
  val realm = "smsgw-test"
  val username = "test"
  val password = "test"

  val commonHeaders = Map(
    "Cache-Control" -> "no-cache"
  )

  val sessionSetup = exec({session => session.set("accessToken", "")})

  val accessToken = exec(http("retrieve_access_token")
    .post(keycloakUrl + "/realms/" + realm + "/protocol/openid-connect/token")
    .headers(commonHeaders)
    .header("Content-Type", "application/x-www-form-urlencoded")
    .formParamMap(Map(
        "grant_type" -> "password",
        "client_id" -> clientId,
        "client_secret" -> clientSecret,
        "username" -> username,
        "password" -> password
      ),
    ).check(status.is(200), jsonPath("$.access_token").saveAs("accessToken"))
     .check(status.is(200), jsonPath("$.refresh_token").saveAs("refreshToken"))
  )

  val refreshToken = exec(http("refresh_access_token")
    .post(keycloakUrl + "/realms/" + realm + "/protocol/openid-connect/token")
    .headers(commonHeaders)
    .header("Content-Type", "application/x-www-form-urlencoded")
    .formParamMap(Map(
      "grant_type" -> "refresh_token",
      "client_id" -> clientId,
      "client_secret" -> clientSecret,
      "refresh_token" -> "${refreshToken}"
    ),
    ).check(status.is(200), jsonPath("$.access_token").saveAs("accessToken"))
  )

  val sendSms = exec(http("send_sms_request")
      .get(smsSenderService)
      .headers(commonHeaders)
      .header("Authorization", "Bearer ${accessToken}")
      .check(status.in(200,401))
      .check(status.saveAs("result"))
  ).doIf(session => session("result").as[Int].equals(401)) {
    refreshToken
  }.pause(requestPause)

  val httpProtocol = http
    .baseUrl("http://localhost:8000/demo")
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")

  val sendSmsScenario = scenario("SendSmsScenario").exec(
    //sessionSetup,
    accessToken,
    repeat(requestCount) {sendSms})

  /*
      .exec({ session =>
      println(session("accessToken").as[String])
      session
    })
  */

  setUp(sendSmsScenario.inject(atOnceUsers(1))).protocols(httpProtocol)
}
